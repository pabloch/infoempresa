package android.prueba.test;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Instrumentation;
import android.content.Intent;
import android.prueba.DetallesEmpresa;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;

/*
 * Clase que implementa los test de la clase DetallesEmpresa
 * 
 */
public class DetallesEmpresaTest extends
		ActivityInstrumentationTestCase2<DetallesEmpresa> {

	private DetallesEmpresa mActivity;
	private TextView nombreView;
	private JSONObject jsonObject;
	private Button boton;
	
	private static final String ID = "2";
	
	public DetallesEmpresaTest()
	{
		super("android.prueba",DetallesEmpresa.class);
	}
	
	/*
	 * Configuracion inicial para cada uno de los test.
	 * 
	 * La clase DetallesEmpresa, es llamada desde la clase InfoEmpresaActitivy 
	 * mediante un objeto Intent.
	 * Por este motivo hay que simular la llamada a dicho objeto antes de obtener
	 * la presente actividad.
	 * 
	 * Despues se obtienen diferentes elementos con los que
	 * se realizaran las pruebas. 
	 * 
	 * Se realiza una vez antes de cada prueba.
	 */
	public void setUp() throws Exception
	{
		super.setUp();
		setActivityInitialTouchMode(false);
		Intent i = new Intent();
		i.setClassName("android.prueba","android.prueba.DetallesEmpresa");
		i.putExtra("id",ID);
		i.putExtra("url", "http://82.194.75.216:8888/SoftwareDeveloper/mercury.0/company");
	    setActivityIntent(i);

		mActivity = getActivity();
		nombreView = (TextView)mActivity.findViewById(android.prueba.R.id.nombre);
		jsonObject = mActivity.getJSONObject();
	    boton = (Button) mActivity.findViewById(android.prueba.R.id.exit);
	}
	
	/*
	 * Test de condiciones iniciales. Solo se realiza una vez.
	 * Comprueba que el campo de texto del nombre de la empresa sea distinto de null
	 * y ademas su valor sea igual a "Tutuah".
	 * 
	 *  Tambien se evalua el tama�o del objeto JSONObject.
	 */
	
	public void testPreConditions()
	{
		assertTrue(nombreView!=null);
		assertTrue(nombreView.getText().equals("Tutuah"));
		assertTrue(jsonObject.length() == 9);
	}
	
	/*
	 * Test de interfaz gr�fica. Comprueba que funciona el boton 
	 * de salida de la actividad. 
	 */
	
	public void testSpinnerUI()
	{
		mActivity.runOnUiThread(new Runnable(){
			public void run() 
			{
				boton.requestFocus();
				boton.setSelected(true);
			}				
		});
	    this.sendKeys(KeyEvent.KEYCODE_DPAD_CENTER);
	}	
	
	/*
	 * Test de estado de actividad. 
	 * Se comprueba que el estado de la actividad
	 * es el mismo tras pausarla.
	 * 
	 * Concretamente se evaluan el tama�o del objeto jsonObject y
	 * de la url de la empresa obtenida, antes y despues de pausar
	 * y se evalua que sean iguales.
	 */
	
	@UiThreadTest
    public void testStatePause() 
	{
	    Instrumentation mInstr = this.getInstrumentation();
	    int tamano = jsonObject.length();
	    String web = ((TextView)mActivity.findViewById(android.prueba.R.id.web)).toString();
	    
	    mInstr.callActivityOnPause(mActivity);
	    mInstr.callActivityOnResume(mActivity);
	    
	    int tamano2 = jsonObject.length();
	    String web2 =((TextView)mActivity.findViewById(android.prueba.R.id.web)).toString();
	    
	    assertEquals(tamano,tamano2);
	    assertEquals(web,web2);
	}
	
	/*
	 * Se comprueba el estado de la actividad tras
	 * haber finalizado.
	 * 
	 *  Concretamente se evalua que el tama�o del objeto
	 *  jsonObject y el tama�o de uno de sus elementos ("phones")
	 *  permanezca inalterable tras finalizar y reiniciar la actividad.
	 */
	
	@UiThreadTest
    public void testStateDestroy() throws JSONException 
	{
	    int tamano = jsonObject.length();
	    int tamanophones = jsonObject.getJSONArray("phones").length();
	    
	    mActivity.finish();
	    mActivity= getActivity();
	    
	    int tamano2 = jsonObject.length();
	    int tamanophones2 = jsonObject.getJSONArray("phones").length();
	    
	    assertEquals(tamano,tamano2);
	    assertEquals(tamanophones,tamanophones2);
    }	
}