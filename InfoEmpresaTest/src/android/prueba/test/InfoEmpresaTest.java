package android.prueba.test;

import android.prueba.InfoEmpresaActivity;
import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;
import android.widget.ListAdapter;
import android.widget.ListView;

/*
 * Clase utilizada para los test de la clase InfoEmpresaActivity 
 */

public class InfoEmpresaTest extends
		ActivityInstrumentationTestCase2<InfoEmpresaActivity> {

	private InfoEmpresaActivity mActivity;
	private ListView mList;
	private ListAdapter mListAdapter;
	
	public static final int NUM_EMPRESAS = 2;
	public static final int POSICION_INICIAL = 0;
	
	public InfoEmpresaTest()
	{
		super("android.prueba",InfoEmpresaActivity.class);
	}
	
	/*
	 * Se obtienen la propia actividad, su ListView
	 * y su ListAdapter.
	 * 
	 * Este metodo se realiza antes de cada test a ejecutar.
	 */
	protected void setUp() throws Exception
	{
		super.setUp();
		setActivityInitialTouchMode(false);

		mActivity = getActivity();
		mList = (ListView) mActivity.getListView();
		mListAdapter = mList.getAdapter();
	}

	/*
	 * Metodo que establece las condiciones iniciales para todos
	 * los test que se realicen.
	 * 
	 * Se comprueba que el escuchador del ListView y el ListAdapter
	 * existen, y que la lista de items del adaptador es igual a
	 * NUM_EMPRESAS.
	 * 
	 * Se realiza solamente una vez, antes de todos los test.
	 */
	
	public void testPreConditions()
	{
		assertTrue(mList.getOnItemClickListener() != null);
		assertTrue(mListAdapter != null);
		assertEquals(mListAdapter.getCount(), NUM_EMPRESAS);
	}
	
	/*
	 * Test de interfaz de usuario.
	 * Se asigna al ListView una posicion inicial y se evalua
	 *  el desplazamiento entre items y su posterior selecci�n.
	 */
	
	public void testSpinnerUI()
	{
		mActivity.runOnUiThread(new Runnable(){
			public void run() 
			{
				mList.requestFocus();
				mList.setSelection(POSICION_INICIAL);
			}				
		});
	    this.sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
	    this.sendKeys(KeyEvent.KEYCODE_DPAD_CENTER);
	}		
}