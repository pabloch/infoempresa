package android.prueba;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/*
 * En esta clase se han definido los metodos estaticos
 * para poderlos utilizar desde cualquier clase.
 *  
 */

public class ObtenerDatos {
	
	/*
	 * Este m�todo realiza una conexion Http a la url proporcionada 
	 * y devuelve un String con el contenido de esa URL.
	 */
	
	public static String obtenerString(String url)
	{
		StringBuilder builder = new StringBuilder();
		HttpClient cliente = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		
		try {
			HttpResponse response = cliente.execute(get);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if(statusCode == 200)
			{
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				
				BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
				String line ="";
				while( (line=buffer.readLine()) != null)
				{
					builder.append(line);
				}
				
			}
			else
			{
				Log.e(InfoEmpresaActivity.class.toString(), "Failed to download file");
			}
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return builder.toString();
	}

	/*
	 * Este m�todo realiza una conexi�n a la url proporcionada,
	 * de la cual obtiene una imagen, la almacena en un objeto Bitmap
	 * y la devuelve a la clase que realiz� la llamada.
	 */
	
	 public static Bitmap cargarFoto(String url)
	 { 
		 HttpURLConnection connection = null;
	     URL miUrl = null;
	     Bitmap imagen = null;
	     
	     /** Intentamos hacer la conexion al servidor */
	     try
	     {
	          miUrl  = new URL(url);
	          connection = null;
	          connection = (HttpURLConnection)miUrl.openConnection();
	          
	          connection.setRequestMethod("GET");
	          connection.setDoOutput(true);
	          connection.setReadTimeout(10000);

	          connection.connect();
	                
	          if(connection.getResponseCode()==200)
		      {
	        	  /** Cargamos la imagen en un bitmap y a su vez en el ImageView */
	        	  InputStream in = connection.getInputStream();	        	  
	        	  imagen = BitmapFactory.decodeStream(in);       	  
		      }	     	          
	     }
	     catch(Exception ex)
	     {	    				
			Log.e("Conexion denegada", ex.getMessage());				
	     }	     
	     return imagen;		 
	 }	
	 
	 /*
	  * Metodo que comprueba la conectividad. Si hay conectividad
	  * devuelve true, sino false.
	  */
	 public static boolean isOnline(Context c) 
	 {
    	 Context  context = c;
    	 ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context .CONNECTIVITY_SERVICE);
    	 if (connectivity == null) 
    	 {
    		 return false;
		 } 
    	 else 
    	 {
    		 NetworkInfo[] info = connectivity.getAllNetworkInfo();
    		 if (info != null) 
    		 {
    			 for (int i = 0; i < info.length; i++) 
    			 {
    				 if (info[i].getState() == NetworkInfo.State.CONNECTED) 
    				 {
    					 return true;
					 }
    			 }
    		 }
    	 }
    	 return false;    	
	 }
	 
}