package android.prueba;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Obtiene los datos de una empresa tras haberla seleccionado
 * previamente en la actividad anterior.
 * Se obtiene un objeto String de una petici�n Http
 * y se pasa a JSONObject, del cual se obtienen los diferentes
 * parametros y se muestran en pantalla.
 */

public class DetallesEmpresa extends Activity{

	private String identificador;
	private String url;
	
	private JSONObject jsonObject;
	
	private int id;
	private String name;
	private String logo;
	private String website;
	private JSONArray addressLines;
	private Integer zipCode;
	private String city;
	private String country;
	private JSONArray phones;
	private Bundle extras;
	
	/*
	 * Recibe de la actividad anterior dos extras que identifican
	 * el id de la empresa a mostrar y la url de la que se
	 * obtuvieron sus datos.
	 */
		
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);         
        setContentView(R.layout.detalles);
        
        extras = getIntent().getExtras();
        identificador = extras.getString("id");
        url = extras.getString("url");
        
        if(ObtenerDatos.isOnline(getApplicationContext()))
        {
        	mostrarDatos();
        }
        else
        {
        	Toast.makeText(getApplicationContext(), "No hay conectividad, por favor conectese a internet",
    				Toast.LENGTH_SHORT).show();
        }
    }

	/*
	 * Actualiza la url para obtener el objeto JSON en forma de String
	 * de los detalles de la empresa elegida, y los asigna a los
	 * diferentes elementos de la interfaz gr�fica.
	 */
	
    public void mostrarDatos()
    {
    	String resultado = ObtenerDatos.obtenerString(url+"/"+identificador);

        try {
			jsonObject = new JSONObject(resultado);
			id = getJSONObject().getInt("id");
			name = getJSONObject().getString("name");
			logo = getJSONObject().getString("logo");
			website = getJSONObject().getString("website");
			addressLines = getJSONObject().getJSONArray("addressLines");					
			zipCode  = getJSONObject().getInt("zipCode");
			city = getJSONObject().getString("city");
			country = getJSONObject().getString("country");
			phones = getJSONObject().getJSONArray("phones");
			
			String direcciones="";
			for(int i=0; i<addressLines.length(); i++)
			{
				direcciones+= addressLines.get(i)+"\n";
			}
			
			String telefonos="";
			for(int i=0; i<phones.length(); i++)
			{
				telefonos+= phones.getJSONObject(i).getString("countryCode")+
						phones.getJSONObject(i).getString("subscriberNumber")+
						"\n";
			}

			TextView nombre = (TextView)findViewById(R.id.nombre);
			nombre.setText(name);
			
			ImageView logotipo = (ImageView)findViewById(R.id.foto);
			
			Bitmap a = ObtenerDatos.cargarFoto(logo);
	        a = Bitmap.createScaledBitmap(a,a.getWidth(),a.getHeight(), false);
			logotipo.setImageBitmap(a);
			
			TextView web = (TextView)findViewById(R.id.web);
			web.setText(website);
			Linkify.addLinks(web, Linkify.WEB_URLS);
			
			TextView dir = (TextView)findViewById(R.id.address);
			dir.setText(direcciones);
			
			TextView postal = (TextView)findViewById(R.id.postal);
			postal.setText(zipCode + " - " + city +" ("+ country + ") ");
			
			TextView telefonosView = (TextView)findViewById(R.id.phones);
			telefonosView.setText(telefonos);
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    }
    
    /*
     * Devuelve el objeto JSONObject
     */
    public JSONObject getJSONObject()
    {
    	return jsonObject;
    }
    
    /*
     * Finaliza la actividad.
     */
    public void finalizar(View v)
    {
    	finish();
    }	
}