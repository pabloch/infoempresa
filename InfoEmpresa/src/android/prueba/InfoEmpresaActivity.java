package android.prueba;


import org.json.JSONArray;
import org.json.JSONException;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Muestra un interfaz de usuario con dos botones.
 * El primero de ellos para refrescar la lista de empresas disponibles
 * y el segundo para abandonar la aplicacion.
 */

public class InfoEmpresaActivity extends ListActivity{
	
	private final String IP="82.194.75.216";
	private final String PORT="8888";
	private String url="http://"+IP+":"+PORT+"/SoftwareDeveloper/mercury.0/company";
	
	private String [] arrayId;
	private String [] arrayName;
	
	protected ArrayAdapter<String> mAdapter;
	protected ListView lv;
	
	/*
	 * Carga el xml inicial y lo rellena la lista 
	 * de empresas siempre y cuando exista conectividad.
	 */
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.main);       
       
        cargarDatos();
    }

    /*
     * Metodo utilizado cuando el usuario pulsa el boton
     * para obtener la lista de empresas. 
     */
    
    public void mostrarLista(View v)
    {
    	cargarDatos();
    }
    
    /*
     * Comprueba la conectividad y en caso afirmativo obtiene los 
     * datos de la url en forma de String.
     * Transforma los datos a JSONObject y los a�ade al ListView
     */
    public void cargarDatos()
    {
    	if(ObtenerDatos.isOnline(getApplicationContext()))
    	{
    		String obtencionJSON = ObtenerDatos.obtenerString(url);
    		try 
    		{
    			JSONArray jsonArray = new JSONArray(obtencionJSON);					
    			arrayId = new String[jsonArray.length()];
    			arrayName = new String[jsonArray.length()];
    			
    			for (int i = 0; i < jsonArray.length(); i++) 
    			{
    				arrayId[i] = jsonArray.getJSONObject(i).getString("id");
    				arrayName[i] = jsonArray.getJSONObject(i).getString("name");
    			}					
    		} 
    		catch (JSONException e) 
    		{
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}		
    		
    		String [] arrayTotal = new String[arrayId.length];
            for(int i=0; i<arrayTotal.length; i++)
            {
            	arrayTotal[i] = arrayId[i] + ".- "+arrayName[i];
            }
            
            mAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.itemempresa, arrayTotal);
            setListAdapter(this.mAdapter);  
            setContentView(R.layout.main);
    		
            lv = getListView();
            lv.setTextFilterEnabled(true);
            lv.setOnItemClickListener(new OnItemClickListener() 
            {
      		    public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
      		    {
      		    	 String idList =(String)((TextView) view).getText();
      		    	 String []pareja = idList.split(".- ");

      		    	 Intent detalles = new Intent(getApplicationContext(),DetallesEmpresa.class);
      		    	 detalles.putExtra("id",pareja[0]);
      		    	 detalles.putExtra("url", url);
      		    	 startActivity(detalles);
      		    }
            });
    	}
    	else
    	{
    		Toast.makeText(getApplicationContext(), "No hay conectividad, por favor conectese a internet",
    				Toast.LENGTH_SHORT).show();
    	}	
    }
    
    /*
     * Finaliza la actividad.
     */
    
    public void finalizar(View v)
    {
    	finish();
    }   
}